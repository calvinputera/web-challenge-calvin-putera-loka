import { useState } from "react";
import { Route, Routes } from "react-router-dom";
import { Context } from "./helpers/Context";
import Login from "./pages/Login";
import Dashboard from "./pages/Dashboard";
import Product from "./pages/Product";

function App() {
	const [authState, setAuthState] = useState(false);
	const [username, setUsername] = useState("");

	return (
		<Context.Provider
			value={{ authState, username, setUsername, setAuthState }}
		>
			<Routes>
				<Route path="/" element={<Login />} />
				<Route path="/dashboard" element={<Dashboard />} />
				<Route path="/dashboard/product/:id" element={<Product />} />
			</Routes>
		</Context.Provider>
	);
}

export default App;
