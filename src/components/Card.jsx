import React from "react";
import { Link } from "react-router-dom";

const Card = ({ id, image, title, brand, price }) => {
	return (
		<div className="bg-white w-full lg:w-1/4 h-auto p-4 rounded-lg border-4 border-primaries flex flex-col gap-3">
			<img src={image} alt="images" className="bg-cover w-full h-52" />
			<div className="flex justify-between">
				<div>
					<p className="font-bold text-primaries">{title}</p>
					<p className="text-third">{brand}</p>
				</div>
				<p className="font-bold text-lg text-secondaries">$ {price}</p>
			</div>
			<Link
				to={`/dashboard/product/${id}`}
				className="bg-primaries p-3 rounded-md text-white font-semibold text-center"
			>
				Detail
			</Link>
		</div>
	);
};

export default Card;
