import React from "react";
import { useNavigate } from "react-router-dom";
import logo from "../assets/images/logo.png";
import { AiOutlineLogout } from "react-icons/ai";

const Navbar = () => {
	const navigate = useNavigate();

	const signOut = () => {
		localStorage.clear();
		navigate("/");
	};

	return (
		<nav className="flex justify-between py-5 px-8 shadow">
			<img src={logo} alt="logo" className="w-14 bg-cover" />

			<div className="flex items-center gap-5">
				<p>{`${localStorage.getItem("firstName")} ${localStorage.getItem(
					"lastName"
				)}`}</p>
				<AiOutlineLogout
					size={25}
					onClick={signOut}
					className="cursor-pointer"
				/>
			</div>
		</nav>
	);
};

export default Navbar;
