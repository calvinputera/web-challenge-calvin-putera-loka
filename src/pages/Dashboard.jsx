import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Navbar from "../components/Navbar";
import { AiOutlineSearch } from "react-icons/ai";
import Card from "../components/Card";

const Dashboard = () => {
	const [dataProducts, setDataProducts] = useState([]);
	const [currentPage, setCurrentPage] = useState(1);
	const [searchInput, setSearchInput] = useState("");
	const [category, setCategory] = useState([]);
	const [nameCategory, setNameCategory] = useState("");

	// const [addTitle, setAddTitle] = useState("");
	// const [addBrand, setAddBrand] = useState("");
	// const [addCategory, setAddCategory] = useState([]);
	// const [addPrice, setAddPrice] = useState(0);
	// const [addStock, setAddStock] = useState(0);

	const itemsPerPage = 12;
	const navigate = useNavigate();

	useEffect(() => {
		const offset = (currentPage - 1) * itemsPerPage;

		if (localStorage.getItem("token") !== null) {
			axios
				.get(
					`https://dummyjson.com/products?limit=${itemsPerPage}&skip=${offset}&select=title,price,stock,category,brand,images`
				)
				.then((resp) => {
					console.log(resp.data.products);
					setDataProducts(resp.data.products);
				})
				.catch((err) => console.log(err));
		} else {
			navigate("/");
		}
	}, [currentPage]);

	const search = () => {
		axios
			.get(`https://dummyjson.com/products/search?q=${searchInput}`)
			.then((resp) => {
				// console.log(resp.data.products);
				setDataProducts(resp.data.products);
			})
			.catch((err) => console.log(err));
	};

	const handleKeyPress = (e) => {
		if (e.keyCode === 13 || e.which === 13) {
			search();
		}
	};

	useEffect(() => {
		axios
			.get("https://dummyjson.com/products/categories")
			.then((resp) => {
				// console.log(resp.data);
				setCategory(resp.data);
			})
			.catch((err) => console.log(err));
	}, []);

	const filterNameCategory = () => {
		axios
			.get(`https://dummyjson.com/products/category/${nameCategory}`)
			.then((resp) => {
				// console.log(resp.data.products);
				setDataProducts(resp.data.products);
			})
			.catch((err) => console.log(err));
	};

	useEffect(() => {
		filterNameCategory();
		// console.log(nameCategory);
	}, [nameCategory]);

	// const addItem = () => {
	// 	axios
	// 		.post("https://dummyjson.com/products/add", {
	// 			title: addTitle,
	// 			brand: addBrand,
	// 			category: addCategory,
	// 			price: addPrice,
	// 			stock: addStock,
	// 		})
	// 		.then((resp) => console.log(resp))
	// 		.catch((err) => console.log(err));
	// };

	const next = () => {
		setCurrentPage(currentPage + 1);
		window.scrollTo({
			top: 0,
			behavior: "smooth",
		});
	};

	return (
		<>
			<Navbar />
			<div className="p-8">
				<div className="flex flex-col w-full gap-3 lg:flex-row lg:justify-end">
					<div className="relative">
						<input
							onKeyPress={handleKeyPress}
							type="text"
							placeholder="Search here"
							className="input input-bordered input-md pr-8 w-full"
							onChange={(e) => setSearchInput(e.target.value)}
						/>
						<div
							className="absolute right-2 top-4 cursor-pointer"
							onClick={search}
						>
							<AiOutlineSearch size={20} />
						</div>
					</div>

					<select
						className="select select-bordered w-full lg:w-40"
						onChange={(e) => setNameCategory(e.target.value)}
					>
						<option disabled selected>
							Category
						</option>
						{category.map((item) => (
							<option value={item}>{item}</option>
						))}
					</select>
				</div>

				{dataProducts.length === 0 ? (
					<div className="w-full flex justify-center items-center py-10">
						<p className="text-3xl font-bold">You're Done 👌</p>
					</div>
				) : (
					<div className="flex flex-wrap gap-14 mt-8 justify-center">
						{dataProducts.map((product) => (
							<Card
								image={product.images[0]}
								title={product.title}
								brand={product.brand}
								price={product.price}
								id={product.id}
							/>
						))}
					</div>
				)}

				<div className="flex gap-5 justify-end mt-8">
					<button
						className="btn bg-primaries text-white"
						onClick={() => setCurrentPage(currentPage - 1)}
						variant={currentPage === 1 ? "bordered" : "solid"}
						disabled={currentPage === 1}
					>
						Previous
					</button>
					<button
						className="btn bg-primaries text-white"
						onClick={next}
						variant={dataProducts.length === 0 ? "bordered" : "solid"}
						disabled={dataProducts.length === 0}
					>
						Next
					</button>
				</div>
			</div>
		</>
	);
};

export default Dashboard;
