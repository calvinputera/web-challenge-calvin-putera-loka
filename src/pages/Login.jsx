import axios from "axios";
import React, { useContext, useState } from "react";
import { Context } from "../helpers/Context";
import { useNavigate } from "react-router-dom";
import logo from "../assets/images/logo.png";
import header from "../assets/images/header-login.png";
import { AiOutlineEye, AiOutlineEyeInvisible } from "react-icons/ai";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer, toast } from "react-toastify";

const Login = () => {
	const [password, setPassword] = useState("");
	const [showPassword, setShowPassword] = useState(false);
	const [isLoading, setIsLoading] = useState(false);

	const { authState, username, setUsername, setAuthState } =
		useContext(Context);

	const navigate = useNavigate();

	// user ID = kminchelle
	// password = 0lelplR

	const signIn = (e) => {
		e.preventDefault();
		setIsLoading(true);
		axios
			.post("https://dummyjson.com/auth/login", {
				username: username,
				password: password,
			})
			.then((resp) => {
				console.log(resp.data);
				localStorage.setItem("token", resp.data.token);
				localStorage.setItem("firstName", resp.data.firstName);
				localStorage.setItem("lastName", resp.data.lastName);
				setAuthState(true);
				if (localStorage.getItem("token") !== null) {
					setIsLoading(false);
					navigate("/dashboard");
				} else {
					setIsLoading(false);
					navigate("/");
				}
			})
			.catch((err) => {
				toast.error("User ID and Password do not match", {
					position: "top-center",
					autoClose: 5000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true,
					progress: undefined,
					theme: "colored",
				});
				setIsLoading(false);
			});
	};

	return (
		<>
			<ToastContainer
				position="top-center"
				autoClose={3000}
				hideProgressBar={false}
				newestOnTop={false}
				closeOnClick
				rtl={false}
				pauseOnFocusLoss
				draggable
				pauseOnHover
				theme="colored"
			/>
			<img src={header} alt="logo" className="w-28" />
			<div className="h-full w-full flex flex-col justify-center items-center">
				<img src={logo} alt="logo" className="w-28 -mt-14" />
				<form className="flex flex-col w-full md:w-1/3 px-12 gap-7 mt-10">
					<div>
						<h1 className="font-semibold text-xl">Login</h1>
						<p className="text-sm">Please sign in to continue</p>
					</div>
					<div className="flex flex-col">
						<label>User ID</label>
						<input
							required
							className="py-4 pr-3 border-b border-b-third focus:outline-none"
							type="text"
							placeholder="user ID"
							onChange={(e) => setUsername(e.target.value)}
						/>
					</div>
					<div className="flex flex-col relative">
						<label>Password</label>
						<input
							required
							className="py-4 pr-3 border-b border-b-third focus:outline-none"
							type={!showPassword ? "password" : "text"}
							placeholder="password"
							onChange={(e) => setPassword(e.target.value)}
						/>
						<div
							className="absolute right-0 top-12 cursor-pointer"
							onClick={() => setShowPassword(!showPassword)}
						>
							{showPassword ? (
								<AiOutlineEye size={20} />
							) : (
								<AiOutlineEyeInvisible size={20} />
							)}
						</div>
					</div>
					<div className="flex justify-end">
						<button
							onClick={signIn}
							className="py-2 px-12 font-medium rounded-full bg-primaries text-white"
						>
							{isLoading ? "loading.." : "LOGIN"}
						</button>
					</div>
				</form>
			</div>
		</>
	);
};

export default Login;
