import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import Navbar from "../components/Navbar";
import { AiTwotoneStar } from "react-icons/ai";

const Product = () => {
	const [title, setTitle] = useState("");
	const [brand, setBrand] = useState("");
	const [thumbnail, setThumbnail] = useState("");
	const [stock, setStock] = useState();
	const [desc, setDesc] = useState("");
	const [rating, setRating] = useState();
	const [price, setPrice] = useState();
	const [discount, setDiscount] = useState();

	const navigate = useNavigate();
	const params = useParams();

	useEffect(() => {
		if (localStorage.getItem("token") !== null) {
			axios
				.get(`https://dummyjson.com/products/${params.id}`)
				.then((resp) => {
					console.log(resp.data);
					setTitle(resp.data.title);
					setBrand(resp.data.brand);
					setThumbnail(resp.data.thumbnail);
					setStock(resp.data.stock);
					setDesc(resp.data.description);
					setRating(resp.data.rating);
					setPrice(resp.data.price);
					setDiscount(resp.data.discountPercentage);
				})
				.catch((err) => console.log(err));
		} else {
			navigate("/");
		}
	}, []);

	return (
		<>
			<Navbar />
			<div className="p-8">
				<p onClick={() => navigate(-1)} className="mb-3 cursor-pointer">
					Back
				</p>
				<div className="lg:flex lg:gap-8">
					<img
						src={thumbnail}
						alt="thumbnail"
						className="w-full bg-cover lg:w-1/2"
					/>
					<div className="flex flex-col justify-between mt-5">
						<div className="flex justify-between">
							<div>
								<h1 className="text-lg font-bold">{title}</h1>
								<h2>{brand}</h2>
								<div className="flex gap-3 mt-4">
									<p className="py-1 px-3 border border-third rounded-md text-xs flex items-center gap-1">
										{rating}{" "}
										<span>
											<AiTwotoneStar size={15} color="#6338A1" />
										</span>
									</p>
									<p className="py-1 px-3 border border-third rounded-md text-xs">
										{stock} pcs
									</p>
								</div>
							</div>
							<div className="flex flex-col items-end lg:items-start">
								<p className="text-2xl font-bold text-secondaries">$ {price}</p>
								<p className="text-xs font-semibold text-primary">
									discount %{discount}
								</p>
							</div>
						</div>
						<div className="mt-4">
							<hr className="border-third" />
							<div className="mt-4 flex flex-col gap-2">
								<p className="text-sm font-semibold">Description</p>
								<p className="text-sm">{desc}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export default Product;
